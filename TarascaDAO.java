package org.tarasca.ardor.contracts;

import nxt.account.Account;
import nxt.addons.AbstractContract;
import nxt.addons.BlockContext;
import nxt.addons.JO;
import nxt.addons.JA;
import nxt.addons.TransactionContext;
import nxt.addons.ValidateContractRunnerIsRecipient;

import nxt.crypto.Crypto;

import nxt.http.callers.GetAssetCall;
import nxt.http.callers.GetBalanceCall;
import nxt.http.responses.TransactionResponse;

import nxt.util.Convert;
import nxt.util.Logger;
import org.json.simple.JSONArray;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class TarascaDAO extends AbstractContract {
    BlockContext contractContext;

    private static final TreeMap<Long, Integer> assetDecimalCache =  new TreeMap<>();

    private long feeRateNQTPerFXT = 0;

    JO transactionMessageJO;

    byte[] privateKeyAssetAccount;
    byte[] privateKeyPaymentAccount;
    String adminPasswordString;

    int ecBlockHeight = 0;
    long ecBlockId = 0;
    int timestampForTransaction = 0;

    public JO processBlock(BlockContext context) {

        contractContext = context;
        JO contractParameters = context.getContractRunnerConfigParams(getClass().getSimpleName());

        transactionMessageJO = new JO();

        int chainId = 2;

        final int assetIssuePeriod = contractParameters.getInt("assetIssuePeriod");
        final int assetTransferHeightOffset = contractParameters.getInt("assetTransferHeightOffset");
        final int assetDividendPaymentHeightOffset = contractParameters.getInt("assetDividendPaymentHeightOffset");

        if(assetTransferHeightOffset <= 0) {
            context.logInfoMessage("configuration error : assetTransferHeightOffset must be greater than zero");
            return new JO();
        }

        if(assetDividendPaymentHeightOffset <= assetTransferHeightOffset) {
            context.logInfoMessage("configuration error : assetDividendPaymentHeightOffset must be greater than assetTransferHeightOffset");
            return new JO();
        }

        int blockHeight = context.getHeight();
        int periodCounterAssetIssue = blockHeight % assetIssuePeriod;
        int periodCounterAssetTransfer = (blockHeight - assetTransferHeightOffset) % assetIssuePeriod;
        int periodCounterDividendPayment = (blockHeight - assetDividendPaymentHeightOffset) % assetIssuePeriod;

        if(periodCounterAssetIssue != 0 && periodCounterAssetTransfer != 0 && periodCounterDividendPayment != 0) {
            context.logInfoMessage("remaining blocks for asset issue : " + (assetIssuePeriod - periodCounterAssetIssue));
            return new JO();
        }

        int evaluationHeightStart = blockHeight - assetIssuePeriod;
        int evaluationHeightEnd = blockHeight;

        if(periodCounterAssetTransfer == 0) {
            evaluationHeightStart -= assetTransferHeightOffset;
            evaluationHeightEnd -= assetTransferHeightOffset;
        }
        else if(periodCounterDividendPayment == 0) {
            evaluationHeightStart -= assetDividendPaymentHeightOffset;
            evaluationHeightEnd -= assetDividendPaymentHeightOffset;
        }

        String feePriorityString = contractParameters.getString("feePriority").toUpperCase();

        String contractNameString = context.getContractName();
        long contractAccount = Convert.parseUnsignedLong(context.getAccount());

        long minFeeRateNQTPerFXT = Convert.parseUnsignedLong(contractParameters.getString("minRateNQTPerFXT"));
        long maxFeeRateNQTPerFXT = Convert.parseUnsignedLong(contractParameters.getString("maxRateNQTPerFXT"));

        JA rates = nxt.http.callers.GetBundlerRatesCall.create()
                .minBundlerBalanceFXT(10)
                .minBundlerFeeLimitFQT(1)
                .transactionPriority(feePriorityString)
                .call()
                .getArray("rates");

        for(JO rate : rates.objects()) {
            if(rate.getInt("chain") == chainId) {
                feeRateNQTPerFXT = Convert.parseUnsignedLong(rate.getString("minRateNQTPerFXT"));

                if(feeRateNQTPerFXT < minFeeRateNQTPerFXT)
                    feeRateNQTPerFXT = minFeeRateNQTPerFXT;

                if (feeRateNQTPerFXT > maxFeeRateNQTPerFXT)
                    feeRateNQTPerFXT = maxFeeRateNQTPerFXT;
            }
        }

        context.logInfoMessage("feeRateNQTPerFXT: " + feeRateNQTPerFXT);

        adminPasswordString = contractParameters.getString("adminPassword");

        if(adminPasswordString == null || adminPasswordString.equals("")) {
            contractContext.logInfoMessage("contract requires admin password (adminPassword)");
            return new JO();
        }

        {
            String privateKeyHexString = contractParameters.getString("privateKeyAssetAccount");

            if (privateKeyHexString == null || privateKeyHexString.length() != 0x40)
                return new JO(); // contract not yet configured

            privateKeyAssetAccount = context.parseHexString(privateKeyHexString);
        }

        {
            String privateKeyHexString = contractParameters.getString("privateKeyPaymentAccount");

            if (privateKeyHexString == null || privateKeyHexString.length() != 0x40)
                return new JO(); // contract not yet configured

            privateKeyPaymentAccount = context.parseHexString(privateKeyHexString);
        }

        long assetId = Convert.parseUnsignedLong(contractParameters.getString("assetId"));
        long assetAccountId =  Account.getId(Crypto.getPublicKey(privateKeyAssetAccount));
        long paymentAccountId =  Account.getId(Crypto.getPublicKey(privateKeyPaymentAccount));

        long reservedBalanceNQTAssetAccount = Convert.parseUnsignedLong(contractParameters.getString("reservedBalanceNQTAssetAccount"));
        long reservedBalanceNQTPaymentAccount = Convert.parseUnsignedLong(contractParameters.getString("reservedBalanceNQTPaymentAccount"));

        JA jsonValidAssetArray = contractParameters.getArray("assetIds");

        SortedSet<Long> assetIdArray = setLongFromJsonStringArray2A(jsonValidAssetArray);
        TreeMap<Long, TreeMap<Long, Long>> accountAssetList = new TreeMap<>();

        // assets may be transferred on other chains and this function may be called again for other chains to include those transactions.
        getReceivedAssetTransfersAndFilterByAssetList(accountAssetList, contractAccount, chainId, evaluationHeightStart, evaluationHeightEnd, assetIdArray, contractNameString);

        long paymentTotalNQT = sumAccountReceivedPayment(paymentAccountId, chainId, evaluationHeightStart, evaluationHeightEnd);
        contractContext.logInfoMessage("period balanceNQT : " + paymentTotalNQT);

        long paymentTotalNQTOverride = chainBalanceNQT(paymentAccountId, chainId);

        if(paymentTotalNQTOverride < paymentTotalNQT) {
            paymentTotalNQT = (long) (paymentTotalNQTOverride * 0.995); //adjusted down to cover transaction fees
            contractContext.logInfoMessage("new period balanceNQT : " + paymentTotalNQT);
        }

        TreeMap<Long, Long> accountsCompleteReceivedAssetSets = new TreeMap<>();

        evaluateAccountsReceivedCompleteAssetSets(accountsCompleteReceivedAssetSets, assetIdArray, accountAssetList);

        ecBlockHeight = evaluationHeightEnd - 1;
        JO response = nxt.http.callers.GetBlockCall.create().height(ecBlockHeight).call();
        timestampForTransaction = response.getInt("timestamp");
        ecBlockId = Convert.parseUnsignedLong(response.getString("block"));

        transactionMessageJO.put("submittedBy", contractNameString);
        transactionMessageJO.put("blockFirst", evaluationHeightStart);
        transactionMessageJO.put("blockLast", (evaluationHeightEnd - 1));

        if(periodCounterAssetIssue == 0) {
            issueAssets(accountsCompleteReceivedAssetSets, chainId, assetId, assetAccountId);
        }

        if(periodCounterAssetTransfer == 0) {
            transferAssets(accountsCompleteReceivedAssetSets, chainId, assetId, assetAccountId);
            paymentSplit(accountsCompleteReceivedAssetSets, chainId, paymentTotalNQT, reservedBalanceNQTPaymentAccount);
        }

        if(periodCounterDividendPayment == 0) {
            dividendsPayment(chainId, assetId, assetAccountId, reservedBalanceNQTAssetAccount, evaluationHeightStart, evaluationHeightEnd);
        }

        return new JO();
    }

    private void dividendsPayment(int chainId, long assetId, long accountId, long reservedBalanceNQT, int heightStartIncluded, int heightEndExcluded) {
        JO response = GetBalanceCall.create(chainId)
                .account(accountId)
                .call();

        long balanceNQT = Convert.parseUnsignedLong(response.getString("balanceNQT"));

        long dividendsQuantityNQT = balanceNQT - reservedBalanceNQT;

        long periodBalanceNQT = sumAccountReceivedPayment(accountId, chainId, heightStartIncluded, heightEndExcluded) - reservedBalanceNQT;

        if(periodBalanceNQT < dividendsQuantityNQT) {
            dividendsQuantityNQT = periodBalanceNQT;
        }

        if(dividendsQuantityNQT <= 0) {
            contractContext.logInfoMessage("dividends : insufficient balance");
            return;
        }

        response = nxt.http.callers.GetAssetCall.create()
                .asset(assetId)
                .call();

        long assetQuantityTotal = Convert.parseUnsignedLong(response.getString("quantityQNT")) / (long) Math.pow(10, getAssetDecimals(assetId));
        long assetBalanceIssuer = assetBalanceFXT(accountId, assetId);

        if(assetQuantityTotal <= assetBalanceIssuer) {
            return;
        }

        long amountNQTPerShare = dividendsQuantityNQT / (assetQuantityTotal - assetBalanceIssuer);

        contractContext.logInfoMessage("dividends NQT : " + amountNQTPerShare + " : " + dividendsQuantityNQT);

        nxt.http.callers.DividendPaymentCall.create(chainId)
                .privateKey(privateKeyAssetAccount)
                .height(contractContext.getHeight())
                .holding(chainId)
                .holdingType((byte) 0)
                .asset(assetId)
                .amountNQTPerShare(amountNQTPerShare)
                .feeRateNQTPerFXT(feeRateNQTPerFXT)
                .ecBlockId(ecBlockId)
                .ecBlockHeight(ecBlockHeight)
                .timestamp(timestampForTransaction)
                .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                .deadline(1440)
                .broadcast(true)
                .call();
    }

    private void transferAssets(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, int chainId, long assetId, long accountId) {

        long quantityAvailable = assetBalanceFXT(accountId, assetId);

        long quantityRequired = sumListLong(accountsCompleteReceivedAssetSets);

        if(quantityRequired <= 0) {
            return;
        }

        if(quantityAvailable < quantityRequired) {
            return;
        }

        for (Long account : accountsCompleteReceivedAssetSets.keySet()) {

            contractContext.logInfoMessage(Convert.rsAccount(account) + " : " + accountsCompleteReceivedAssetSets.get(account) * (long) Math.pow(10, getAssetDecimals(assetId)));

            nxt.http.callers.TransferAssetCall.create(chainId)
                    .privateKey(privateKeyAssetAccount)
                    .recipient(account)
                    .asset(assetId)
                    .quantityQNT(accountsCompleteReceivedAssetSets.get(account) * (long) Math.pow(10, getAssetDecimals(assetId)))
                    .feeRateNQTPerFXT(feeRateNQTPerFXT)
                    .ecBlockId(ecBlockId)
                    .ecBlockHeight(ecBlockHeight)
                    .timestamp(timestampForTransaction)
                    .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                    .deadline(1440)
                    .broadcast(true)
                    .call();
        }
    }

    private void paymentSplit(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, int chainId, long amountNQT, long reservedBalanceNQT) {

        long splitTotal = sumListLong(accountsCompleteReceivedAssetSets);

        if(splitTotal < 1) {
            return;
        }

        long totalNQT = amountNQT - reservedBalanceNQT;

        if(totalNQT < splitTotal) {
            return;
        }

        for (Long account : accountsCompleteReceivedAssetSets.keySet()) {

            long paymentNQT = BigInteger.valueOf(accountsCompleteReceivedAssetSets.get(account)).multiply(BigInteger.valueOf(totalNQT)).divide(BigInteger.valueOf(splitTotal)).longValue();

            contractContext.logInfoMessage(Convert.rsAccount(account) + " : paymentNQT : " + paymentNQT);

            nxt.http.callers.SendMoneyCall.create(chainId)
                    .privateKey(privateKeyPaymentAccount)
                    .recipient(account)
                    .amountNQT(paymentNQT)
                    .feeRateNQTPerFXT(feeRateNQTPerFXT)
                    .ecBlockId(ecBlockId)
                    .ecBlockHeight(ecBlockHeight)
                    .timestamp(timestampForTransaction)
                    .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                    .deadline(1440)
                    .broadcast(true)
                    .call();
        }
    }

    private void issueAssets(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, int chainId, long assetId, long accountId) {

        long quantityAvailable = assetBalanceFXT(accountId, assetId);

        long quantityRequired = sumListLong(accountsCompleteReceivedAssetSets);

        long quantityIssue = quantityRequired - quantityAvailable;

        contractContext.logInfoMessage("quantityAvailable : " + quantityAvailable + ", quantityRequired : " + quantityRequired + ", issueAssets : " + quantityIssue);

        if(quantityIssue <= 0) {
            return;
        }

        nxt.http.callers.IncreaseAssetSharesCall.create(chainId)
                .privateKey(privateKeyAssetAccount)
                .asset(assetId)
                .quantityQNT(quantityIssue * (long) Math.pow(10, getAssetDecimals(assetId)))
                .feeRateNQTPerFXT(feeRateNQTPerFXT)
                .ecBlockId(ecBlockId)
                .ecBlockHeight(ecBlockHeight)
                .timestamp(timestampForTransaction)
                .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                .deadline(1440)
                .broadcast(true)
                .call();
    }

    private void evaluateAccountsReceivedCompleteAssetSets(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, SortedSet<Long> assetList, TreeMap<Long, TreeMap<Long, Long>> accountAssetList) {

        for (Long account : accountAssetList.keySet()) {
            TreeMap<Long, Long> assetQuantityList = accountAssetList.get(account);

            if(! accountsCompleteReceivedAssetSets.containsKey(account)) {
                accountsCompleteReceivedAssetSets.put(account, (long) -1);
            }

            for (Long assetId : assetList) {
                long quantityFXT = 0;

                if (assetQuantityList.containsKey(assetId)) {
                    quantityFXT =  assetQuantityList.get(assetId) / (long) Math.pow(10, getAssetDecimals(assetId));
                }

                long accountMinimumQuantityFXT = accountsCompleteReceivedAssetSets.get(account);

                if (accountMinimumQuantityFXT < 0 || accountMinimumQuantityFXT > quantityFXT) {
                    accountsCompleteReceivedAssetSets.put(account, quantityFXT);
                }

                if (quantityFXT <= 0) {
                    contractContext.logInfoMessage(Convert.rsAccount(account) + " : incomplete set");
                    break;
                }
            }
        }
    }

    private void getReceivedAssetTransfersAndFilterByAssetList(TreeMap<Long, TreeMap<Long, Long>> accountAssetList, long recipient, int chainId, int heightStartIncluded, int heightEndExcluded, SortedSet<Long> assetList, String contractNameString) {

        for(int height = heightStartIncluded; height < heightEndExcluded; height++) {

            JO response = nxt.http.callers.GetExecutedTransactionsCall.create(chainId)
                    .adminPassword(adminPasswordString)
                    .recipient(recipient)
                    .height(height)
                    .type(2).subtype(1)
                    .call();

            JA arrayOfTransactions = response.getArray("transactions");

            for (Object transactionObject : arrayOfTransactions) {
                JO transactionJO = (JO)transactionObject;

                JO attachment = transactionJO.getJo("attachment");

                if (attachment == null)
                    continue;

                if(!attachment.isExist("messageIsText")) {
                    continue;
                }

                if (!attachment.getBoolean("messageIsText"))
                    continue;

                String message = attachment.getString("message");

                if (message == null)
                    continue;

                JO messageAsJson = null;

                try {
                    messageAsJson = JO.parse(message);
                } catch (IllegalArgumentException e) {/* empty */}

                if (messageAsJson == null)
                    continue;

                if (!messageAsJson.isExist("contract") || !messageAsJson.getString("contract").equals(contractNameString))
                    continue;

                long assetId = Convert.parseUnsignedLong(attachment.getString("asset"));

                if (!assetList.contains(assetId)) {
                    continue;
                }

                TreeMap<Long, Long> assetQuantityList;
                long sender = Convert.parseUnsignedLong(transactionJO.getString("sender"));
                long assetQuantityQNT = Convert.parseUnsignedLong(attachment.getString("quantityQNT"));

                if (!accountAssetList.containsKey(sender)) {
                    assetQuantityList = new TreeMap<>();
                } else {
                    assetQuantityList = accountAssetList.get(sender);
                }

                if (assetQuantityList.containsKey(assetId)) {
                    assetQuantityQNT += assetQuantityList.get(assetId);
                }

                assetQuantityList.put(assetId, assetQuantityQNT);
                accountAssetList.put(sender, assetQuantityList);
            }
        }
    }

    private long sumAccountReceivedPayment(long recipient, int chainId, int heightStartIncluded, int heightEndExcluded) {

        long paymentTotal = 0;

        for(int height = heightStartIncluded; height < heightEndExcluded; height++) {

            JO response = nxt.http.callers.GetExecutedTransactionsCall.create(chainId)
                    .adminPassword(adminPasswordString)
                    .recipient(recipient)
                    .height(height)
                    .type(0).subtype(0)
                    .call();

            JA arrayOfTransactions = response.getArray("transactions");

            for (Object transactionObject : arrayOfTransactions) {
                JO transactionJO = (JO)transactionObject;

                long amountNQT = Long.parseUnsignedLong(transactionJO.getString("amountNQT"));

                paymentTotal += amountNQT;
            }
        }

        return paymentTotal;
    }

    byte[] privateKey;
    String secretForRandomSerialString;
    String secretForRandomString;
    SecureRandom random;

    int chainId = 2;

    private static final TreeMap<Long, Integer> assetIssueQuantityCache =  new TreeMap<>();

    TransactionContext transactionContext;

    @ValidateContractRunnerIsRecipient
    public JO processTransaction(TransactionContext context) {
        JO params = context.getContractRunnerConfigParams(getClass().getSimpleName());

        transactionContext = context;
        transactionMessageJO = new JO();

        {
            // bypass to allow explicit transaction deadline etc. until there is a standard way to override all default transaction parameters.

            String privateKeyHexString = params.getString("privateKey");

            if (privateKeyHexString == null || privateKeyHexString.length() != 0x40)
                return new JO(); // contract not yet configured

            privateKey = context.parseHexString(privateKeyHexString);
        }

        JA jsonValidAssetArray = params.getArray("assetIds");

        String validCurrencyString = params.getString("validCurrency");
        long priceAsCurrency = Convert.parseUnsignedLong(params.getString("priceAsCurrencyNQT"));

        long assetPackCostChainNQT = Convert.parseUnsignedLong(params.getString("priceAsChainNQT"));

        JA paymentAccountArray = params.getArray("paymentAccountRS");
        JA paymentAccountFractionArray = params.getArray("paymentFractionFloat");

        List<Long> listPaymentAccount = null;
        List<Double> listPaymentSplit = null;

        if(paymentAccountArray != null && paymentAccountFractionArray != null) {
            listPaymentAccount = listAccountIdFromRSArray(paymentAccountArray);
            listPaymentSplit = listDoubleFromJA(paymentAccountFractionArray);
        } else {
            transactionContext.logInfoMessage("paymentAccountRS not configured");
        }

        long refundThresholdNQT = 100000000;

        if(params.containsKey("refundThresholdNQT")) {
            refundThresholdNQT = Convert.parseUnsignedLong(params.getString("refundThresholdNQT"));
        }

        long refundExtraFeeNQT = 10000000;

        if(params.containsKey("refundThresholdNQT")) {
            refundExtraFeeNQT = Convert.parseUnsignedLong(params.getString("refundExtraFeeNQT"));
        }

        long assetPackSize = Convert.parseUnsignedLong(params.getString("assetPackSize"));

        String feePriorityString = params.getString("feePriority").toUpperCase();

        TransactionResponse triggerTransaction = context.getTransaction();

        long assetPacksQuantity = 0;
        long acceptedChainNQT = 0;
        long remainingChainNQT = 0;

        int transactionSubType = triggerTransaction.getSubType();
        int transactionType = triggerTransaction.getType();

        switch(transactionType) {

            case 5: {
                if (transactionSubType == 3) {
                    assetPacksQuantity = packQuantityFromCurrencyPayment(triggerTransaction, validCurrencyString, priceAsCurrency);
                }

                break;
            }

            case 0: {
                if (transactionSubType == 0 && triggerTransaction.getChainId() == chainId) {
                    long receivedChainNQT =  triggerTransaction.getAmount();
                    assetPacksQuantity = receivedChainNQT / assetPackCostChainNQT;
                    acceptedChainNQT = assetPacksQuantity * assetPackCostChainNQT;
                    remainingChainNQT = receivedChainNQT - acceptedChainNQT;
                }

                break;
            }

            default: {
                context.logInfoMessage("unhandled transaction type (%d:%d)", transactionType, transactionSubType);
                return new JO();
            }
        }

        long minFeeRateNQTPerFXT = Convert.parseUnsignedLong(params.getString("minRateNQTPerFXT"));
        long maxFeeRateNQTPerFXT = Convert.parseUnsignedLong(params.getString("maxRateNQTPerFXT"));

        JA rates = nxt.http.callers.GetBundlerRatesCall.create()
                .minBundlerBalanceFXT(10)
                .minBundlerFeeLimitFQT(1)
                .transactionPriority(feePriorityString)
                .call()
                .getArray("rates");

        for(JO rate : rates.objects()) {
            if(rate.getInt("chain") == chainId) {
                feeRateNQTPerFXT = Convert.parseUnsignedLong(rate.getString("minRateNQTPerFXT"));

                if(feeRateNQTPerFXT < minFeeRateNQTPerFXT) {
                    feeRateNQTPerFXT = minFeeRateNQTPerFXT;
                }

                if (feeRateNQTPerFXT > maxFeeRateNQTPerFXT) {
                    feeRateNQTPerFXT = maxFeeRateNQTPerFXT;
                }
            }
        }

        context.logInfoMessage("feeRateNQTPerFXT: " + feeRateNQTPerFXT);

        prepareRandom(context);

        transactionMessageJOAppendContext(context);

        {
            long refundAmountNQT = remainingChainNQT - refundExtraFeeNQT;

            if (refundAmountNQT >= refundThresholdNQT) {
                contractSendMoney(privateKey, context, refundAmountNQT, chainId, triggerTransaction.getSenderId(), feeRateNQTPerFXT);
            }
        }

        context.logInfoMessage("number of asset packs: %d", assetPacksQuantity);

        if (assetPacksQuantity == 0) {
            return new JO();
        }

        TreeMap<Long, Long> assetIdWeightMap = mapLongLongFromJsonStringArray(jsonValidAssetArray);

        TreeMap<Long, Double> assetMap = new TreeMap<>();
        double assetTotal = assetCumulativeWeightListAppend(assetMap, assetIdWeightMap);

        Logger.logInfoMessage("assets : " +  assetTotal + " : " + assetMap.size());

        if(assetTotal == 0) {
            return new JO();
        }

        List<Double> assetProbability = new ArrayList<>();
        List<Long> assetList = new ArrayList<>();

        generateCumulativeProbabilityFromAssetMap(assetProbability, assetList, assetMap, assetTotal);

        HashMap<Long, Long> assetPick = new HashMap<>();

        long assetTransferTotal = assetPacksQuantity * assetPackSize;

        pickAssets(assetPick, assetTransferTotal, assetProbability, assetList);

        contractTransferAssetFromHashMap(privateKey, context, triggerTransaction.getSenderId(), chainId, feeRateNQTPerFXT, assetPick);

        broadcastPaymentSplit(listPaymentAccount, listPaymentSplit, acceptedChainNQT);

        return context.getResponse();
    }

    private void prepareRandom(TransactionContext context) {
        JO params = context.getContractRunnerConfigParams(getClass().getSimpleName());

        try {
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }

        MessageDigest digest = Crypto.sha256();

        secretForRandomString = params.getString("secretForRandomString");
        secretForRandomSerialString = params.getString("secretForRandomSerialString");

        byte[] secretForRandom;
        byte[] secretForRandomSerial;

        if(secretForRandomString != null) {
            secretForRandom = secretForRandomString.getBytes(StandardCharsets.UTF_8);
            digest.update(secretForRandom);
        }

        if(secretForRandomSerialString != null) {
            secretForRandomSerial = secretForRandomSerialString.getBytes(StandardCharsets.UTF_8);
            digest.update(secretForRandomSerial);
        }

        // random seed derived from long from HASH(secretForRandomString | secretForRandomSerialString | getBlockId | getFullHash)
        digest.update(ByteBuffer.allocate(Long.BYTES).putLong(context.getBlock().getBlockId()).array());
        digest.update(context.getTransaction().getFullHash());

        long derivedSeedForAssetPick = ByteBuffer.wrap(digest.digest(), 0, 8).getLong();
        random.setSeed(derivedSeedForAssetPick);
    }

    private void generateCumulativeProbabilityFromAssetMap(List<Double> assetProbability, List<Long> assetList, TreeMap<Long, Double> assetMap, double assetTotal) {
        double probabilityCumulativeTotal = 0;

        for (Map.Entry<Long, Double> e : assetMap.entrySet()) {
            Long asset = e.getKey();
            Double quantity = e.getValue();

            double probability = quantity / assetTotal;
            probabilityCumulativeTotal += probability;

            assetProbability.add(probabilityCumulativeTotal);
            assetList.add(asset);
        }
    }

    private void pickAssets(HashMap<Long, Long> assetPickList, long assetTransferTotal, List<Double> assetProbability, List<Long> assetList) {

        for(long index = 0; index < assetTransferTotal; index++) {
            double nextDouble = random.nextDouble();
            long asset = getRandomAsset(assetProbability, assetList, nextDouble);

            Logger.logInfoMessage("random : " + nextDouble + " : asset :  " + Long.toUnsignedString(asset));

            long quantity = (long) (1 * Math.pow(10, getAssetDecimals(asset)));

            if (assetPickList.containsKey(asset)) {
                quantity += assetPickList.get(asset);
            }

            assetPickList.put(asset, quantity);
        }
    }

    private void broadcastPaymentSplit(List<Long> listAccounts, List<Double> listFraction, long totalAmountNQT) {

        if(listAccounts == null || listFraction == null)
            return;

        long sender = Convert.parseAccountId(transactionContext.getAccount());

        int count = listAccounts.size();

        for(int i = 0; i < count; i++) {
            long recipient = listAccounts.get(i);
            double fraction = listFraction.get(i);

            if (recipient == sender)
                continue;

            long amountNQT = (long) ((double)totalAmountNQT * fraction); // NOTE sender needs extra balance to support variable fee

            if (amountNQT <= 0) {
                continue;
            }

            contractSendMoney(privateKey, transactionContext, amountNQT, chainId, recipient, feeRateNQTPerFXT);
        }
    }

    private void contractTransferAssetFromHashMap(byte[] privateKey, TransactionContext context, long recipient, int chainId, long feeRateNQTPerFXT, HashMap<Long, Long> assetPickList) {

        for (Map.Entry<Long, Long> e : assetPickList.entrySet()) {
            Long asset = e.getKey();
            Long quantityQNT = e.getValue();

            contractTransferAsset(privateKey, context, asset, quantityQNT, chainId, recipient, feeRateNQTPerFXT);
        }
    }

    private void contractSendMoney(byte[] privateKey, TransactionContext context, long amountNQT, int chainId, long recipient, long feeRateNQTPerFXT) {

        if(recipient == Convert.parseAccountId(context.getAccount())) {
            return;
        }

        context.logInfoMessage(Convert.rsAccount(recipient) + " : amountNQT : " + amountNQT);

        JO response = nxt.http.callers.SendMoneyCall.create(chainId)
                .privateKey(privateKey)
                .recipient(recipient)
                .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                .ecBlockHeight(context.getTransaction().getHeight())
                .ecBlockId(context.getTransaction().getBlockId())
                .timestamp(context.getTransaction().getTimestamp())
                .deadline(1440)
                .feeRateNQTPerFXT(feeRateNQTPerFXT)
                .amountNQT(amountNQT)
                .broadcast(true)
                .call();

        Logger.logInfoMessage(response.toJSONString());
    }

    private void contractTransferAsset(byte[] privateKey, TransactionContext context, long asset, long quantityQNT, int chainId, long recipient, long feeRateNQTPerFXT) {

        JO response = nxt.http.callers.TransferAssetCall.create(chainId)
                .privateKey(privateKey)
                .recipient(recipient)
                .message(transactionMessageJO.toJSONString()).messageIsText(true).messageIsPrunable(true)
                .ecBlockHeight(context.getTransaction().getHeight())
                .ecBlockId(context.getTransaction().getBlockId())
                .timestamp(context.getTransaction().getTimestamp())
                .deadline(1440)
                .feeRateNQTPerFXT(feeRateNQTPerFXT)
                .asset(asset)
                .quantityQNT(quantityQNT)
                .broadcast(true)
                .call();

        Logger.logInfoMessage(response.toJSONString());
    }

    private int getAssetIssueQuantity(long assetId) {
        int assetIssueQuantity = 0;

        boolean cached = false;

        synchronized (assetIssueQuantityCache) {
            if(assetIssueQuantityCache.containsKey(assetId)) {
                cached = true;
                assetIssueQuantity = assetIssueQuantityCache.get(assetId);
            }
        }

        if(!cached) {
            JO jsonResponse = GetAssetCall.create().asset(assetId).call();

            if(jsonResponse.containsKey("errorCode")) {
                transactionContext.logInfoMessage("error : asset not found : " + assetId + "\n" + jsonResponse.toJSONString());
            }

            assetIssueQuantity = jsonResponse.getInt("quantityQNT");

            synchronized (assetIssueQuantityCache) {
                assetIssueQuantityCache.put(assetId, assetIssueQuantity);
            }
        }

        return assetIssueQuantity;
    }

    private long packQuantityFromCurrencyPayment(TransactionResponse response, String currency, long priceQNTCurrency) {
        long unitsQNT = 0;

        JO attachment = response.getAttachmentJson();
        String txcurrency = attachment.getString("currency");

        if (txcurrency.equals(currency)) {
            unitsQNT = attachment.getInt("unitsQNT") / priceQNTCurrency;
        }

        return unitsQNT;
    }

    private void transactionMessageJOAppendContext(TransactionContext context) {
        transactionMessageJO.put("submittedBy", context.getContractName());
        transactionMessageJO.put("transactionTrigger", Convert.toHexString(context.getTransaction().getFullHash()));

        if(secretForRandomString != null && secretForRandomSerialString != null)
            transactionMessageJO.put("serialForRandom", secretForRandomSerialString);
    }

    private long getRandomAsset(List<Double> listProbability, List<Long> listAsset, double randomValue){
        long asset = 0;

        for(int index = 0; index < listProbability.size(); index++)
            if (listProbability.get(index) >= randomValue) {
                asset = listAsset.get(index);
                break;
            }

        return asset;
    }

    private double assetCumulativeWeightListAppend(TreeMap<Long, Double> list, TreeMap<Long, Long> assetWeightMap) {

        int countOfAssets = assetWeightMap.size();

        if(countOfAssets == 0)
            return 0;

        double weightTotal = 0;

        for (long assetId: assetWeightMap.keySet()) {
            long assetWeight = assetWeightMap.get(assetId);

            double weight = assetWeight / Math.pow(10, getAssetDecimals(assetId));

            list.put(assetId, weight);
            weightTotal += weight;
        }

        return weightTotal;
    }

    private List<Double> listDoubleFromJA(JA jsonValueArray) {

        int jsonArraySize = jsonValueArray.toJSONArray().size();

        if( jsonArraySize == 0) {
            return null;
        }

        List<Double> list = new ArrayList<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Double.parseDouble((jsonValueArray.toJSONArray().get(i).toString())));
        }

        return list;
    }

    private List<Long> listAccountIdFromRSArray(JA jsonStringArray) {

        int jsonArraySize = jsonStringArray.toJSONArray().size();

        if( jsonArraySize == 0) {
            return null;
        }

        List<Long> list = new ArrayList<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Convert.parseAccountId((jsonStringArray.toJSONArray().get(i).toString())));
        }

        return list;
    }

    private long assetBalanceFXT(long accountId, long assetId) {
        long quantity = 0;

        JO response = nxt.http.callers.GetAccountAssetsCall.create()
                .asset(assetId)
                .account(accountId)
                .call();

        if(response.isExist("quantityQNT")) {
            quantity = Convert.parseUnsignedLong(response.getString("quantityQNT")) / (long) Math.pow(10, getAssetDecimals(assetId));
        }

        return quantity;
    }

    private long chainBalanceNQT(long accountId, int chainId) {
        long amountNQT = 0;

        JO response = nxt.http.callers.GetBalanceCall.create()
                .account(accountId)
                .chain(chainId)
                .call();

        if(response.isExist("balanceNQT")) {
            amountNQT = Convert.parseUnsignedLong(response.getString("balanceNQT"));
        }

        return amountNQT;
    }

    private long sumListLong(TreeMap<Long, Long> map) {
        long sum = 0;

        for (Long key : map.keySet()) {
            sum += map.get(key);
        }

        return sum;
    }

    private int getAssetDecimals(long assetId) {
        int assetDecimal = 0;

        boolean cached = false;

        synchronized (assetDecimalCache) {
            if(assetDecimalCache.containsKey(assetId)) {
                cached = true;
                assetDecimal = assetDecimalCache.get(assetId);
            }
        }

        if(!cached) {
            assetDecimal = nxt.http.callers.GetAssetCall.create().asset(assetId).call().getInt("decimals");

            synchronized (assetDecimalCache) {
                assetDecimalCache.put(assetId, assetDecimal);
            }
        }

        return assetDecimal;
    }

    private SortedSet<Long> setLongFromJsonStringArray(JA jsonStringArray) {

        int jsonArraySize = jsonStringArray.toJSONArray().size();

        if( jsonArraySize == 0)
            return null;

        SortedSet<Long> list = new TreeSet<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Long.parseUnsignedLong(jsonStringArray.toJSONArray().get(i).toString()));
        }

        return list;
    }

    private SortedSet<Long> setLongFromJsonStringArray2A(JA jsonStringArrayJA) {
        JSONArray jsonStringArray = jsonStringArrayJA.toJSONArray();

        int jsonArraySize = jsonStringArray.size();

        if( jsonArraySize == 0)
            return null;

        SortedSet<Long> list = new TreeSet<>();

        for(int i = 0; i < jsonArraySize; i++) {
            JSONArray array = (JSONArray) jsonStringArray.get(i);
            list.add(Long.parseUnsignedLong(array.get(0).toString()));
        }

        return list;
    }

    public static TreeMap<Long, Long> mapLongLongFromJsonStringArray(JA jsonStringArrayJA) {
        JSONArray jsonStringArray = jsonStringArrayJA.toJSONArray();

        int jsonArraySize = jsonStringArray.size();

        if( jsonArraySize == 0)
            return null;

        TreeMap<Long, Long> list = new TreeMap<>();

        for(int i = 0; i < jsonArraySize; i++) {
            JSONArray array = (JSONArray) jsonStringArray.get(i);
            list.put(Long.parseUnsignedLong(array.get(0).toString()), Long.parseUnsignedLong(array.get(1).toString()));
        }

        return list;
    }
}
