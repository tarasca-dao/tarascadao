import org.json.simple.JSONObject;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.TreeMap;

public class ArdorApi {
    private RestApi restApi;

    ArdorApi(String hostProtocolString, String hostNameString, String hostPortString) {
        restApi = new RestApi(hostProtocolString, hostNameString, hostPortString);
    }

    private static final TreeMap<Long, Integer> assetDecimalCache =  new TreeMap<>();

    public void setHostProtocolString(String hostProtocolString) {
        restApi.setHostProtocolString(hostProtocolString);
    }

    public void setHostNameString(String hostNameString) {
        restApi.setHostNameString(hostNameString);
    }

    public void setHostPortString(String hostPortString) {
        restApi.setHostPortString(hostPortString);
    }

    public JSONObject jsonObjectHttpApi(boolean methodIsPost, HashMap<String, String> parameters) throws IOException {
        return restApi.jsonObjectHttpApi(methodIsPost, parameters);
    }

    public byte[] getAccountPublicKey(long accountId) throws IOException {
        byte[] publicKey = null;

        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("account", Long.toUnsignedString(accountId));

        parameters.put("requestType", "getAccount");
        JSONObject response = restApi.jsonObjectHttpApi(true, parameters);

        if(response.containsKey("publicKey")) {
            publicKey = bytesFromHexString((String) response.get("publicKey"));
        }
        else {
            System.out.println(response.toJSONString());
        }

        return publicKey;
    }

    public void transactionBytesBroadcast(byte[] transactionBytes, String prunableAttachmentJSONString) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        hashMapAddByteArrayAsHexStringParameter(parameters, "transactionBytes", transactionBytes);

        if(prunableAttachmentJSONString != null) {
            parameters.put("prunableAttachmentJSON", prunableAttachmentJSONString);
        }

        parameters.put("requestType", "broadcastTransaction");
        JSONObject response = restApi.jsonObjectHttpApi(true, parameters);

        if(response.containsKey("errorCode")) {
            System.out.println(response.toJSONString());
            System.exit(1);
        }
    }

    public long transactionResponseCalculateFeeFQT(JSONObject jsonTransactionResponse) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("transactionJSON", ((JSONObject) jsonTransactionResponse.get("transactionJSON")).toJSONString());

        parameters.put("requestType", "calculateFee");
        JSONObject response = restApi.jsonObjectHttpApi(true, parameters);

        if(!response.containsKey("feeNQT")) {
            System.out.println(response.toJSONString());
            System.exit(1);
        }

        return Long.parseUnsignedLong((String) response.get("feeNQT"));
    }

    public int getAssetDecimals(long assetId) throws IOException {

        synchronized (assetDecimalCache) {
            int assetDecimal;

            if (assetDecimalCache.containsKey(assetId)) {
                assetDecimal = assetDecimalCache.get(assetId);
            } else {

                HashMap<String, String> parameters = new HashMap<>();

                parameters.put("asset", Long.toUnsignedString(assetId));
                parameters.put("requestType", "getAsset");
                JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

                int result = 0;

                assetDecimal = (int) JsonSimpleFunctions.getLong(response, "decimals", result); // undetected error

                assetDecimalCache.put(assetId, assetDecimal);
            }

            return assetDecimal;
        }
    }

    public String getAssetName(long parameter) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("asset", Long.toUnsignedString(parameter));
        parameters.put("requestType", "getAsset");
        JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

        return (String) response.get("name");
    }

    public String getAccountRsFromString(String parameter) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("account", parameter);
        parameters.put("requestType", "getAccount");
        JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

        return (String) response.get("accountRS");
    }

    public long getAccountIdFromString(String parameter) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("account", parameter);
        parameters.put("requestType", "getAccount");
        JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

        long result = 0;

        return JsonSimpleFunctions.getLongFromStringUnsigned(response, "account", result);
    }

    public int getBlockHeight() throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("requestType", "getBlockchainStatus");
        JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

        int result = 0;

        return (int) JsonSimpleFunctions.getLong(response, "numberOfBlocks", result);
    }

    private static void hashMapAddByteArrayAsHexStringParameter(HashMap<String, String> hashMap, String keyString, byte[] byteParameter) {
        hashMap.put(keyString, String.format("%0" + (byteParameter.length << 1) + "x", new BigInteger(1, byteParameter)));
    }

    public static byte[] bytesFromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
