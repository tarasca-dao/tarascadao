import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class TarascaDaoContractMonitor {
    private static final HashMap<String, String> applicationResult = new HashMap<>();

    private static ArdorApi ardorApi;
    private static RestApi restApi;

    private static class Configuration {
        private String fileName = "configuration.json";

        private String hostString = "localhost";
        private String portString = "27876";
        private String protocolString = "http";
        private String adminPassword = null;

        /*
        private long accountAsset = 0;
        private long accountPayment = 0;
        private long accountDividends = 0;
        */

        private int assetIssuePeriod = 10080;
        private int blockHeightOverride = 0;
        private int periodNegativeOffset = 0;

        SortedSet<Long> assetIdArray = new TreeSet<>();

        boolean oldContractTrigger = false;

        String contractNameString = "TarascaDAO";
        String assetAccountString = "ARDOR-4V3B-TVQA-Q6LF-GMH3T";
        long contractAccount;

        int chainId = 2;

        boolean formatAsJson = true;

        int availableProcessors = 2;
    }

    private static final Configuration configuration = new Configuration();

    public static void main(String[] args) throws Exception {

        if (args.length > 1) {
            applicationResult.put("errorDescription", "usage : configuration");
        } else {

            if(args.length > 0) {
                configuration.fileName = args[0];
            }

            configurationRead(configuration.fileName);
        }

        process();

        JSONObject applicationResultJson = new JSONObject();

        for(String key : applicationResult.keySet()) {
         applicationResultJson.put(key, applicationResult.get(key));
        }

        System.out.println(applicationResultJson.toJSONString());

        System.exit(0);
    }

    private static void configurationRead(String filePath) throws IOException, ParseException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            applicationResult.put("errorDescription", "empty configuration : " + filePath);
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));

        configuration.formatAsJson = JsonSimpleFunctions.getBoolean(jsonConfiguration, "formatAsJson", configuration.formatAsJson);

        if(jsonConfiguration.containsKey("node")) {
            JSONObject jsonObject = (JSONObject) jsonConfiguration.get("node");

            configuration.hostString = JsonSimpleFunctions.getString(jsonObject,"host", configuration.hostString);
            configuration.portString = JsonSimpleFunctions.getString(jsonObject,"port", configuration.portString);
            configuration.protocolString = JsonSimpleFunctions.getString(jsonObject,"protocol", configuration.protocolString);
            configuration.adminPassword = JsonSimpleFunctions.getString(jsonObject,"adminPassword", configuration.adminPassword);
        }

        ardorApi = new ArdorApi(configuration.protocolString, configuration.hostString, configuration.portString);
        restApi = new RestApi(configuration.protocolString, configuration.hostString, configuration.portString);

        if(jsonConfiguration.containsKey("contract")) {
            JSONObject jsonObject = (JSONObject) jsonConfiguration.get("contract");

            if(jsonObject.containsKey("assetIds")) {
                JSONArray jsonValidAssetArray = (JSONArray) jsonObject.get("assetIds");
                configuration.assetIdArray = JsonSimpleFunctions.setLongFromJsonStringArray(jsonValidAssetArray);
            }

            configuration.assetIssuePeriod = (int) JsonSimpleFunctions.getLong(jsonObject, "assetIssuePeriod", configuration.assetIssuePeriod);
            configuration.assetAccountString = JsonSimpleFunctions.getString(jsonObject, "assetAccount", configuration.assetAccountString);
            configuration.contractNameString = JsonSimpleFunctions.getString(jsonObject, "contractName", configuration.contractNameString);
            configuration.chainId = (int) JsonSimpleFunctions.getLong(jsonObject, "chain", configuration.chainId);

            configuration.oldContractTrigger = JsonSimpleFunctions.getBoolean(jsonObject, "oldContractTrigger", configuration.oldContractTrigger);

            configuration.contractAccount = ardorApi.getAccountIdFromString(configuration.assetAccountString);
        }

        configuration.availableProcessors = Runtime.getRuntime().availableProcessors();

        if(jsonConfiguration.containsKey("monitor")) {
            JSONObject jsonObject = (JSONObject) jsonConfiguration.get("monitor");

            configuration.blockHeightOverride = (int) JsonSimpleFunctions.getLong(jsonObject, "blockHeightOverride", configuration.blockHeightOverride);
            configuration.periodNegativeOffset = (int) JsonSimpleFunctions.getLong(jsonObject, "periodNegativeOffset", configuration.periodNegativeOffset);
        }
    }

    private static void process() throws IOException, InterruptedException, ClassNotFoundException {

        int blockHeight = ardorApi.getBlockHeight();
        int blockHeightOverride = ardorApi.getBlockHeight();

        if(configuration.blockHeightOverride > 0) {
            blockHeightOverride = configuration.blockHeightOverride - (configuration.periodNegativeOffset * configuration.assetIssuePeriod);
        }

        if(blockHeightOverride < blockHeight) {
            blockHeight = blockHeightOverride;
        }

        if(blockHeight < 0) {
            blockHeight = 0;
        }

//        int periodRemainingForAssetIssue = configuration.assetIssuePeriod - (blockHeight % configuration.assetIssuePeriod);

        int evaluationPeriodIndex = blockHeight / configuration.assetIssuePeriod;
        int evaluationHeightStartIncluded = evaluationPeriodIndex * configuration.assetIssuePeriod;
        int evaluationHeightEndExcluded = evaluationHeightStartIncluded + configuration.assetIssuePeriod;

        if(evaluationHeightEndExcluded > blockHeight + 1) {
            evaluationHeightEndExcluded = blockHeight + 1;
        }

        TreeMap<Long, TreeMap<Long, Long>> accountAssetList = new TreeMap<>();
        buildAccountAssetList(accountAssetList, evaluationHeightStartIncluded, evaluationHeightEndExcluded, blockHeight);

        TreeMap<Long, Long> accountsCompleteReceivedAssetSets = new TreeMap<>();
        TreeMap<Long, Long> accountsIncompleteReceivedAssetSets = new TreeMap<>();

        evaluateAccountsReceivedCompleteAssetSets(accountsCompleteReceivedAssetSets, accountsIncompleteReceivedAssetSets, configuration.assetIdArray, accountAssetList);

        JSONObject jsonObjectSets = jsonObjectEvaluatedSets(accountsCompleteReceivedAssetSets, accountsIncompleteReceivedAssetSets);

        applicationResult.put("blockHeight", Integer.toString(blockHeight));
        applicationResult.put("evaluationHeightStart", Integer.toString(evaluationHeightStartIncluded));
        applicationResult.put("evaluationHeightEnd", Integer.toString(evaluationHeightEndExcluded - 1));
        applicationResult.put("nextHeightStart", Integer.toString(evaluationHeightStartIncluded + configuration.assetIssuePeriod));

        if(configuration.formatAsJson) {
            applicationResult.put("evaluation", jsonObjectSets.toJSONString());
        } else {
            printEvaluation(accountsCompleteReceivedAssetSets, accountsIncompleteReceivedAssetSets);
        }
    }

    private static void buildAccountAssetList(TreeMap<Long, TreeMap<Long, Long>> accountAssetList, int evaluationHeightStartIncluded, int evaluationHeightEndExcluded, int blockHeight) throws InterruptedException, ClassNotFoundException, IOException {

        int evaluationHeightEndSafe = blockHeight - 721;
        int evaluationHeightCached = evaluationHeightStartIncluded;

        if(evaluationHeightEndSafe < evaluationHeightStartIncluded) {
            evaluationHeightEndSafe = evaluationHeightStartIncluded;
        }

        String fileName = "evaluationCacheFile." + evaluationHeightStartIncluded;
        FileInputStream fileInputStream = null;

        try {
            fileInputStream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) { /* empty */ }

        if(fileInputStream != null) {
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            evaluationHeightCached = objectInputStream.readInt();
            TreeMap<Long, TreeMap<Long, Long>> accountAssetListCached = (TreeMap<Long, TreeMap<Long, Long>>) objectInputStream.readObject();
            mergeAccountAssetLists(accountAssetList, accountAssetListCached);
        }

        if(evaluationHeightCached < evaluationHeightEndSafe) {
            appendAccountAssetListThreaded(accountAssetList, evaluationHeightCached, evaluationHeightEndSafe);
            evaluationHeightCached = evaluationHeightEndSafe;
        }

        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeInt(evaluationHeightEndSafe);
        objectOutputStream.writeObject(accountAssetList);

        appendAccountAssetListThreaded(accountAssetList, evaluationHeightCached, evaluationHeightEndExcluded);
    }

    private static void appendAccountAssetListThreaded(TreeMap<Long, TreeMap<Long, Long>> accountAssetList, int evaluationHeightStartIncluded, int evaluationHeightEndExcluded) throws IOException, InterruptedException {
        if(evaluationHeightEndExcluded <= evaluationHeightStartIncluded) {
            return;
        }

        int blockCountTotal = evaluationHeightEndExcluded - evaluationHeightStartIncluded;
        int blockCountPerThread = blockCountTotal / configuration.availableProcessors;

        if(blockCountPerThread < 25) {
            blockCountPerThread = 25;
        }

        List<Thread> threadList = new ArrayList<>();
        List<ContractEvaluation> contractEvaluationList = new ArrayList<>();

        for(int i = evaluationHeightStartIncluded; i <= evaluationHeightEndExcluded; i += blockCountPerThread) {
            int startHeightIncluded = i;
            int endHeightExcluded = i + blockCountPerThread;

            if(endHeightExcluded > evaluationHeightEndExcluded) {
                endHeightExcluded = evaluationHeightEndExcluded;
            }

            if(endHeightExcluded <= startHeightIncluded) {
                continue;
            }

            ContractEvaluation contractEvaluation = new ContractEvaluation(configuration.contractAccount, configuration.chainId, startHeightIncluded, endHeightExcluded, configuration.assetIdArray, configuration.contractNameString);

            Thread thread = new Thread(contractEvaluation);
            threadList.add(thread);
            contractEvaluationList.add(contractEvaluation);

            thread.start();
        }

        for (int i = 0; i < threadList.size(); i ++) {
            threadList.get(i).join();
            mergeAccountAssetLists(accountAssetList, contractEvaluationList.get(i).accountAssetList);
        }
    }

    private static void mergeAccountAssetLists(TreeMap<Long, TreeMap<Long, Long>> destinationList, TreeMap<Long, TreeMap<Long, Long>> sourceList) {

        for(Long key : sourceList.keySet()) {
            TreeMap<Long, Long> subListSource = sourceList.get(key);
            TreeMap<Long, Long> subListDestination;

            if(destinationList.containsKey(key)) {
                subListDestination = destinationList.get(key);
            } else {
                subListDestination = new TreeMap<>();
            }

            mergeLongLongLists(subListDestination, subListSource);

            destinationList.put(key, subListDestination);
        }
    }

    private static void mergeLongLongLists(TreeMap<Long, Long> destinationList, TreeMap<Long, Long> sourceList) {

        for(Long key : sourceList.keySet()) {
            Long longSource = sourceList.get(key);
            Long longDestination;

            if(destinationList.containsKey(key)) {
                longDestination = destinationList.get(key);
            } else {
                longDestination = new Long(0);
            }

            longDestination += longSource;

            destinationList.put(key, longDestination);
        }
    }

    private static void printEvaluation(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, TreeMap<Long, Long> accountsIncompleteReceivedAssetSets) throws IOException {

        for(long account : accountsIncompleteReceivedAssetSets.keySet()) {
            long assetId = accountsIncompleteReceivedAssetSets.get(account);
            System.out.println(ardorApi.getAccountRsFromString(Long.toUnsignedString(account)) + " : 0 : next missing  : " + Long.toUnsignedString(assetId) + " \"" + ardorApi.getAssetName(assetId) + "\"");
        }

        long totalAssets = 0;

        for(long account : accountsCompleteReceivedAssetSets.keySet()) {

            if(accountsCompleteReceivedAssetSets.get(account) < 1) {
                continue;
            }

            System.out.println(ardorApi.getAccountRsFromString(Long.toUnsignedString(account)) + " : " + accountsCompleteReceivedAssetSets.get(account));
            totalAssets += accountsCompleteReceivedAssetSets.get(account);
        }

        System.out.println("Accounts : " + accountsCompleteReceivedAssetSets.size());
        System.out.println("Assets   : " + totalAssets);
    }

    private static JSONObject jsonObjectEvaluatedSets(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, TreeMap<Long, Long> accountsIncompleteReceivedAssetSets) throws IOException {
        JSONObject jsonObjectResult = new JSONObject();

        JSONArray jsonArrayCompleteSet = new JSONArray();
        jsonObjectResult.put("accountsValidSet", jsonArrayCompleteSet);

        for(long account : accountsCompleteReceivedAssetSets.keySet()) {

            if(accountsCompleteReceivedAssetSets.get(account) < 1) {
                continue;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accountRs", ardorApi.getAccountRsFromString(Long.toUnsignedString(account)));
            jsonObject.put("quantity", accountsCompleteReceivedAssetSets.get(account));
            jsonArrayCompleteSet.add(jsonObject);
        }

        JSONArray jsonArrayIncompleteSet = new JSONArray();
        jsonObjectResult.put("accountsIncompleteSet", jsonArrayIncompleteSet);

        for(long account : accountsIncompleteReceivedAssetSets.keySet()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accountRs", ardorApi.getAccountRsFromString(Long.toUnsignedString(account)));
            jsonArrayIncompleteSet.add(jsonObject);
        }

        return jsonObjectResult;
    }

    private static void evaluateAccountsReceivedCompleteAssetSets(TreeMap<Long, Long> accountsCompleteReceivedAssetSets, TreeMap<Long, Long> accountsIncompleteReceivedAssetSets, SortedSet<Long> assetList, TreeMap<Long, TreeMap<Long, Long>> accountAssetList) throws IOException {

        for (Long account : accountAssetList.keySet()) {
            TreeMap<Long, Long> assetQuantityList = accountAssetList.get(account);

            if(! accountsCompleteReceivedAssetSets.containsKey(account)) {
                accountsCompleteReceivedAssetSets.put(account, (long) -1);
            }

            for (Long assetId : assetList) {
                long quantityFXT = 0;

                if (assetQuantityList.containsKey(assetId)) {
                    quantityFXT =  assetQuantityList.get(assetId) / (long) Math.pow(10, ardorApi.getAssetDecimals(assetId));
                }

                long accountMinimumQuantityFXT = accountsCompleteReceivedAssetSets.get(account);

                if (accountMinimumQuantityFXT < 0 || accountMinimumQuantityFXT > quantityFXT) {
                    accountsCompleteReceivedAssetSets.put(account, quantityFXT);
                }

                if (quantityFXT <= 0) {
                    accountsIncompleteReceivedAssetSets.put(account, assetId);
                    break;
                }
            }
        }
    }

    public static class ContractEvaluation implements Runnable{
        private final long recipient;
        private final int chainId;
        private final int heightStartIncluded;
        private final int heightEndExcluded;
        private final SortedSet<Long> assetList;
        private final String contractNameString;
        public final TreeMap<Long, TreeMap<Long, Long>> accountAssetList = new TreeMap<>();

        public ContractEvaluation(long recipient, int chainId, int heightStartIncluded, int heightEndExcluded, SortedSet<Long> assetList, String contractNameString) {
            this.recipient = recipient;
            this.chainId = chainId;
            this.heightStartIncluded = heightStartIncluded;
            this.heightEndExcluded = heightEndExcluded;
            this.assetList = assetList;
            this.contractNameString = contractNameString;
        }

        public void run() {
            try {
                getReceivedAssetTransfersAndFilterByAssetList(accountAssetList, recipient, chainId, heightStartIncluded, heightEndExcluded, assetList, contractNameString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void getReceivedAssetTransfersAndFilterByAssetList(TreeMap<Long, TreeMap<Long, Long>> accountAssetList, long recipient, int chainId, int heightStartIncluded, int heightEndExcluded, SortedSet<Long> assetList, String contractNameString) throws IOException {
            JSONParser jsonParser = new JSONParser();

            HashMap<String, String> parameters = new HashMap<>();

            if(configuration.adminPassword != null && configuration.adminPassword.length() > 0) {
                parameters.put("adminPassword", configuration.adminPassword);
            }

            parameters.put("recipient", Long.toUnsignedString(recipient)); // generalized
            parameters.put("chain", Integer.toString(chainId));
            parameters.put("type", "2");
            parameters.put("subtype", "1");
            parameters.put("requestType", "getExecutedTransactions");

            parameters.put("height", "0");

            for(int height = heightStartIncluded; height < heightEndExcluded; height++) {
                parameters.remove("height");
                parameters.put("height", Integer.toString(height));

                JSONObject response = restApi.jsonObjectHttpApi(false, parameters);

                JSONArray arrayOfTransactions = (JSONArray) response.get("transactions");

                if(arrayOfTransactions == null) {
                    continue;
                }

                for (Object transactionObject : arrayOfTransactions) {
                    JSONObject transactionJSONObject = (JSONObject) transactionObject;

                    JSONObject attachment = (JSONObject) transactionJSONObject.get("attachment");

                    if (attachment == null)
                        continue;

                    if(!configuration.oldContractTrigger) {
                        if (!attachment.containsKey("messageIsText")) {
                            continue;
                        }

                        if (!(boolean) attachment.get("messageIsText"))
                            continue;

                        String message = (String) attachment.get("message");

                        if (message == null)
                            continue;

                        JSONObject messageAsJson;

                        try {
                            messageAsJson = (JSONObject) jsonParser.parse(message);
                        } catch (IllegalArgumentException | ParseException e) {
                            continue;
                        }

                        if (messageAsJson == null)
                            continue;

                        if (!(messageAsJson.get("contract")).equals(contractNameString)) {
                            continue;
                        }
                    } else {
                        if (attachment.containsKey("messageIsText")) {
                            if ((boolean) attachment.get("messageIsText")) {
                                String message = (String) attachment.get("message");

                                if (message != null) {
                                    JSONObject messageAsJson = null;

                                    try {
                                        messageAsJson = (JSONObject) jsonParser.parse(message);
                                    } catch (IllegalArgumentException | ParseException e) { /* empty */ }

                                    if(messageAsJson != null) {
                                        if ((messageAsJson.containsKey("contract") && (!messageAsJson.get("contract").equals(contractNameString)))) {
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    long assetId = Long.parseUnsignedLong((String) attachment.get("asset"));

                    if (!assetList.contains(assetId)) {
                        continue;
                    }

                    TreeMap<Long, Long> assetQuantityList;
                    long sender = Long.parseUnsignedLong((String) transactionJSONObject.get("sender"));
                    long assetQuantityQNT = Long.parseUnsignedLong((String) attachment.get("quantityQNT"));

                    if (!accountAssetList.containsKey(sender)) {
                        assetQuantityList = new TreeMap<>();
                    } else {
                        assetQuantityList = accountAssetList.get(sender);
                    }

                    if (assetQuantityList.containsKey(assetId)) {
                        assetQuantityQNT += assetQuantityList.get(assetId);
                    }

                    assetQuantityList.put(assetId, assetQuantityQNT);
                    accountAssetList.put(sender, assetQuantityList);
                }
            }
        }
    }
}
