import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class RestApi {
    private String hostProtocolString;
    private String hostNameString;
    private String hostPortString;

    RestApi(String hostProtocolString, String hostNameString, String hostPortString) {
        this.hostProtocolString = hostProtocolString;
        this.hostNameString = hostNameString;
        this.hostPortString = hostPortString;
    }

    public JSONObject jsonObjectHttpApi(boolean methodIsPost, HashMap<String, String> parameters) throws IOException {
        StringBuilder urlStringBuilder = new StringBuilder();

        urlStringBuilder.append(hostProtocolString).append("://").append(hostNameString).append(":").append(hostPortString).append("/nxt");

        HttpURLConnection connection;

        if(methodIsPost) {
            URL url = new URL(urlStringBuilder.toString());

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setReadTimeout(8000);

            StringBuilder stringBuilder = new StringBuilder();

            if(parameters != null) {
                for (Object parameterObject : parameters.keySet()) {
                    String parameterString = (String) parameterObject;
                    stringBuilder.append("&").append(parameterString).append("=").append(parameters.get(parameterString));
                }
            }

            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            DataOutputStream dos = new DataOutputStream(connection.getOutputStream());

            dos.writeBytes(stringBuilder.toString());
        }
        else {
            boolean first = true;

            if(parameters != null) {
                for (Object parameterObject : parameters.keySet()) {

                    String parameterString = (String) parameterObject;

                    if(first) {
                        urlStringBuilder.append("?");
                        first = false;
                    } else {
                        urlStringBuilder.append("&");
                    }

                    urlStringBuilder.append(parameterString).append("=").append(parameters.get(parameterString));
                }
            }

            URL url = new URL(urlStringBuilder.toString());

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(8000);

            connection.connect();
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();

        String line = bufferedReader.readLine();

        while (line != null)
        {
            stringBuilder.append(line).append("\n");
            line = bufferedReader.readLine();
        }

        return (JSONObject) JSONValue.parse(stringBuilder.toString());
    }

    public void setHostProtocolString(String hostProtocolString) {
        this.hostProtocolString = hostProtocolString;
    }

    public void setHostNameString(String hostNameString) {
        this.hostNameString = hostNameString;
    }

    public void setHostPortString(String hostPortString) {
        this.hostPortString = hostPortString;
    }
}
