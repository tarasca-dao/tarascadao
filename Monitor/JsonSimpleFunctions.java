import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.SortedSet;
import java.util.TreeSet;

public class JsonSimpleFunctions {
    public static String getString(JSONObject jsonObject, String key, String defaultValue) {
        String result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (String) jsonObject.get(key);
            }
        }

        return result;
    }

    public static boolean getBoolean(JSONObject jsonObject, String key, boolean defaultValue) {
        boolean result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (boolean) jsonObject.get(key);
            }
        }

        return result;
    }

    public static int getInt(JSONObject jsonObject, String key, int defaultValue) {
        int result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (int) jsonObject.get(key);
            }
        }

        return result;
    }

    public static long getLong(JSONObject jsonObject, String key, int defaultValue) {
        long result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (long) jsonObject.get(key);
            }
        }

        return result;
    }

    public static long getLongFromStringUnsigned(JSONObject jsonObject, String key, long defaultValue) {
        long result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = Long.parseUnsignedLong((String) jsonObject.get(key));
            }
        }

        return result;
    }

    public static SortedSet<Long> setLongFromJsonStringArray(JSONArray jsonStringArray) {

        int jsonArraySize = jsonStringArray.size();

        if( jsonArraySize == 0)
            return null;

        SortedSet<Long> list = new TreeSet<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Long.parseUnsignedLong(jsonStringArray.get(i).toString()));
        }

        return list;
    }
}
